FROM python:3.11.4-alpine
LABEL maintainer="Mohamed Amir Abidi"
ENV PYTHONUNBUFFERED 1

EXPOSE 8000
WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
COPY . /app
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]