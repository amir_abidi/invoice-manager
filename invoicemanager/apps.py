"""
This file is used to configure the app name for the project."""
from __future__ import unicode_literals

from django.apps import AppConfig

class InvoiceManagerConfig(AppConfig):
    name = 'invoicemanager'
